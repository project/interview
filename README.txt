Interview module for Drupal 4.6

This module provides an interview type content. The drupal's administrator
will be able to publish full interviews which several common sections for a
content type like this: title, subtitle, lead, highlight and footer_image.

Steps to get this module running on drupal:

	1) Import interview.mysql from mysql
		+ mysql -u drupal_user -p drupal_db < interview.mysql
	2) Install, enable and configure interview module
		+ move interview directory to /{drupal_installation_path}/modules/
	3) You need to install image.module to get 'footer_image' field working.
